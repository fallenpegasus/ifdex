#
# makefile for `ifdex'
#
VERS=$(shell sed <ifdex -n -e '/version *= *"\(.*\)"/s//\1/p')

SOURCES = README COPYING NEWS ifdex ifdex.txt ifdex.1 Makefile \
	control ifdex-logo.png

all: ifdex.1

.SUFFIXES: .html .txt .1

# Requires asciidoc and xsltproc/docbook stylesheets.
.txt.1:
	a2x --doctype manpage --format manpage -D . $<
.txt.html:
	a2x --doctype manpage --format xhtml -D . $<

clean:
	rm -f *~ *.1 *.html docbook-xsl.css

install: ifdex.1 uninstall
	install -m 0755 -d $(DESTDIR)/usr/bin
	install -m 0755 -d $(DESTDIR)/usr/share/man/man1
	install -m 0755 ifdex $(DESTDIR)/usr/bin/
	install -m 0644 ifdex.1 $(DESTDIR)/usr/share/man/man1/

uninstall:
	rm -f /usr/bin/ifdex /usr/share/man/man6/ifdex.1
	rm -f /usr/share/pixmaps/ifdex.png

PYLINTOPTS = --rcfile=/dev/null --reports=n \
	--msg-template="{path}:{line}: [{msg_id}({symbol}), {obj}] {msg}" \
	--dummy-variables-rgx='^_'
SUPPRESSIONS = "C0103,C0301"
pylint:
	@pylint $(PYLINTOPTS) --disable=$(SUPPRESSIONS) ifdex


check:
	@./ifdex -c smoketest.tst | diff -u smoketest.chk -
	@echo "No diff output is good news."

rebuild:
	./ifdex -c smoketest.tst >smoketest.chk

version:
	@echo $(VERS)

ifdex-$(VERS).tar.gz: $(SOURCES)
	@ls $(SOURCES) | sed s:^:ifdex-$(VERS)/: >MANIFEST
	@(cd ..; ln -s ifdex ifdex-$(VERS))
	(cd ..; tar -czf ifdex/ifdex-$(VERS).tar.gz `cat ifdex/MANIFEST`)
	@ls -l ifdex-$(VERS).tar.gz
	@(cd ..; rm ifdex-$(VERS))

dist: ifdex-$(VERS).tar.gz

release: ifdex-$(VERS).tar.gz ifdex.html
	shipper version=$(VERS) | sh -e -x

refresh: ifdex.html
	shipper -N -w version=$(VERS) | sh -e -x
